package Ronin;

import java.util.Set;

import Core.Field;
import Core.Window;
import Ronin.player;

public class Main {

	public static Field f = new Field();
	public static Window w = new Window(f);
	public static void main(String[] args) throws InterruptedException{
		// TODO Auto-generated method stub
			
		player pla = new player("Ronin", 0, 0, 50, 50, 0, "resources/papyrus.png", f);
		enemy e1 = new enemy("enemy1", 1300, 550, 1350, 600, 0, "resources/enemigo.gif", f);
		enemy e2 = new enemy("enemy2", 600, 400, 650, 450, 0, "resources/enemigo.gif", f);

		
		while(true){
			Thread.sleep(30);

			f.lockScroll(pla, w);

			f.scroll(300, 300);
			Set<Character> keys = input(pla);
			pla.move(keys);
			e1.IAY(900, 600, pla);
			e2.IAX(900, 600, pla);

			f.draw();
		}
	}
	public static Set<Character> input(player pla){
		return w.getPressedKeys();
	}
}
