package Ronin;

import java.util.Set;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class player extends PhysicBody{

	public player(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		// TODO Auto-generated constructor stub
	}
	public void move(Set<Character> keys){
		if(keys.contains('d')){
			this.setVelocity(5, 0);
			this.path = "resources/megawalk.gif";
		}
		else if(keys.contains('w')){
			this.setVelocity(0, -5);
			this.path = "resources/megawalk.gif";
		}
		else if(keys.contains('a')){
			this.setVelocity(-5, 0);
			this.path = "resources/megawalk.gif";
		}
		else if(keys.contains('s')){
			this.setVelocity(0, 5);
			this.path = "resources/megawalk.gif";
		}
		else if(keys.contains('d') && keys.contains('w')){
			this.setVelocity(3, -3);
		}
		else {
			this.setVelocity(0, 0);
			this.path = "resources/papyrus.png";
		}
	}
	@Override
	public void onCollisionEnter(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
		
	}
}
