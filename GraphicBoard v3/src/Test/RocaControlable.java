package Test;

import Core.Field;

public class RocaControlable extends Roca implements Constants{

	public RocaControlable(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f,
			int accions) {
		super(name, x1, y1, x2, y2, angle, path, f, accions);
	}

	public void input(Input in) {

		switch (in) {
		case AMUNT:
			this.y1 -= GAP;
			this.y2 -= GAP;
			break;
		case AVALL:
			this.y1 += GAP;
			this.y2 += GAP;
			break;
		case DRETA:
			this.x1 += GAP;
			this.x2 += GAP;
			break;
		case ESQUERRA:
			this.x1 -= GAP;
			this.x2 -= GAP;
			break;
		case GRAN:
			break;
		case PETIT:
			break;
		}
	}
}
