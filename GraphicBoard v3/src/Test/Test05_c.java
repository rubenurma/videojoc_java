package Test;

import java.util.ArrayList;
import java.util.Set;

import Core.Field;
import Core.Window;


public class Test05_c implements Constants{
	static Field f = new Field();
	static Window w = new Window(f, 1600, 880);
	
	public static void main(String[] args) throws InterruptedException {
		Roca terra = new Roca("Terra", 50, 800, 1500, 850, 0, "resources\\swap.png", f, 0);
		Roca plataforma1 = new Roca("Plataforma", 500, 680, 600, 690, 0, "resources\\swap.png", f, 0);
		Roca plataforma2 = new Roca("Plataforma", 300, 520, 400, 530, 0, "resources\\swap.png", f, 0);
		Roca plataforma3 = new Roca("Plataforma", 500, 420, 600, 430, 0, "resources\\swap.png", f, 0);
		Personatge_5 p1 = new Personatge_5("link", 50, 250, 50 + MIDAX, 250 + MIDAY, 0, "resources/link1.gif", f);
		p1.flippedX=false;
		p1.setConstantForce(0, 0.2);
		
		while (true) {
			f.draw();
			Thread.sleep(20);
			Set<Character> keys = input();
			p1.move(keys);
			Set<Character> keysd = inputd();
			
			if (keysd.contains(' '))
				p1.disparar();
			if (keysd.contains('w'))
				p1.jump();
		}
	}

	private static Set<Character> input() {
		return w.getPressedKeys();
	}

	private static Set<Character> inputd() {
		return w.getKeysDown();
	}
}
