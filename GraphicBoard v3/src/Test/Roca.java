package Test;

import Core.Field;
import Core.Sprite;

public class Roca extends Sprite{
	public static int comptador = 0;
	
	private int accionsDisponibles;
	
	public Roca(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		comptador ++;
	}

	public Roca(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, int accions) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.accionsDisponibles = accions;
		comptador ++;
	}
}
